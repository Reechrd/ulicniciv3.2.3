import React, {ChangeEventHandler, useState} from "react";
import classes from "./input.module.scss"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";

type TProps = {
    type: "text" | "password",
    placeholder?: string,
    content: string,
    onChange: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>,
}

const Input = (props: TProps) => {
    const [toggle, setToggle] = useState(false);

    const ToggleHide = () => {
        setToggle(!toggle)
    }

    return (
        <div className={classes.input_root}>
            <input type={props.type === "text" ? props.type : toggle ? "text" : "password"} className={classes.input} placeholder={props.placeholder} onChange={props.onChange}
                   value={props.content}/>
            {props.type === "password" &&
            <div className={classes.icon_button} onClick={ToggleHide}>
                {toggle ? <VisibilityIcon/>:<VisibilityOffIcon/>}
            </div>
            }
        </div>
    )
}

export default Input