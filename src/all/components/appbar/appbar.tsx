import React from "react";
import AppbarTop from "./appbar_top";
import AppbarSide from "./appbar_side";

const Appbar = () => {
    return (
        <>
            <AppbarTop/>
            <AppbarSide/>
        </>
    )
}

export default Appbar