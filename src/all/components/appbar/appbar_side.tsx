import React from "react";
import classes from "./appbar.module.scss"
import HomeIcon from '@mui/icons-material/Home';
import UpdateIcon from '@mui/icons-material/Update';
import EmailIcon from '@mui/icons-material/Email';
import {Beenhere, ContactSupport, EmojiEvents, Equalizer, Gavel, Group} from '@mui/icons-material';
import IconButton from "../buttons/icon_button/icon_button";
import {useSelector} from "react-redux";
import clsx from "clsx";
import {RootState} from "../../redux/reducer";
import SendIcon from '@mui/icons-material/Send';

const AppbarSide = () => {

    const expanded = useSelector((state: RootState)=>state.appbar.expanded)
    const notification = useSelector((state: RootState)=>state.account.notification)

    return (
        <div className={clsx(classes.side, expanded && classes.expanded)}>
            <IconButton url={"/"} label={"Domů"}>
                <HomeIcon fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Plán"}>
                <UpdateIcon fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/messages"} badge={notification ? " " : undefined} label={"Zprávy"}>
                <EmailIcon fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/rules"} label={"Pravidla"}>
                <Gavel fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Statistiky"}>
                <Equalizer fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Uživatelé"}>
                <Group fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Prestiž"}>
                <EmojiEvents fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Hlasování"}>
                <Beenhere fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Fórum"}>
                <ContactSupport fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
            <IconButton url={"/todo"} label={"Kontakt"}>
                <SendIcon fontSize={"large"} style={{color: "white"}}/>
            </IconButton>
        </div>
    )
}

export default AppbarSide;