import React from "react";
import classes from "./appbar.module.scss"
import IconButton from "../buttons/icon_button/icon_button";
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../redux/reducer";
import {toggleAppbarExpanded, toggleAppbarMenu} from "../../redux/action_creators";
import {RouteComponentProps, withRouter} from "react-router-dom";
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import Menu from "./menu/menu";

const AppbarTop = (props: RouteComponentProps) => {
    const expanded = useSelector((state: RootState)=> state.appbar.expanded)
    const dispatch = useDispatch();

    return (
        <div className={classes.top}>
            <div className={classes.button} onClick={()=>dispatch(toggleAppbarExpanded())}>
                <IconButton>
                    {expanded ? <ChevronLeftIcon fontSize={"large"} style={{color: "white"}}/> : <ChevronRightIcon fontSize={"large"} style={{color: "white"}}/>}
                </IconButton>
            </div>
            <div className={classes.text} onClick={()=> {
                props.history.push("/")
            }}>
                ULIČNÍCI
            </div>
            <div className={classes.button} onClick={()=>dispatch(toggleAppbarMenu())}>
                <IconButton>
                    <PermIdentityIcon fontSize={"medium"} style={{color: "white"}}/>
                </IconButton>
                <Menu />
            </div>
        </div>
    )
}

export default withRouter(AppbarTop);