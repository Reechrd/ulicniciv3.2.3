import React, {useEffect, useRef} from "react";
import classes from "./menu.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../../redux/reducer";
import {
    setAccountKey,
    setAccountMail,
    setAccountName,
    setAccountTag,
    toggleAppbarMenu
} from "../../../redux/action_creators";
import TextButton from "../../buttons/text_button/text_button";
import {RouteComponentProps, withRouter} from "react-router-dom";

export const useClickAwayListener = (ref: React.RefObject<HTMLElement>, onClickAway: () => void) => {
    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (event.button === 0 && ref.current && !ref.current.contains(event.target as Node)) {
                onClickAway()
            }
        }

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [onClickAway, ref]);
}

const Menu = (props: RouteComponentProps) => {
    const ref = useRef<HTMLDivElement | null>(null);
    const menu = useSelector((state: RootState)=>state.appbar.menu)
    const dispatch = useDispatch();
    const key = useSelector((state:RootState)=>state.account.key)

    useClickAwayListener(ref, ()=>{menu && dispatch(toggleAppbarMenu())})

    if(menu){
        return (
            <div className={classes.root} ref={ref}>
                {
                    key !== "" ?
                        <>
                            <TextButton label={"Můj účet"} onClick={()=> {
                                props.history.push("/account")
                            }}/>
                            <TextButton label={"Odhlásit se"} onClick={()=> {
                                dispatch(setAccountKey(""))
                                dispatch(setAccountName(""))
                                dispatch(setAccountMail(""))
                                dispatch(setAccountTag(""))
                                props.history.push("/")
                            }}/>
                        </>
                        :
                        <>
                        <TextButton label={"Přihlásit se"} onClick={()=> {
                            props.history.push("/login")
                        }}/>
                        <TextButton label={"Registrovat se"} onClick={()=> {
                            props.history.push("/register")
                        }}/>
                    </>

                }
            </div>
        )
    }else{
        return null
    }
}

export default withRouter(Menu);