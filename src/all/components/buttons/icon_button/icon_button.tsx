import React from "react";
import classes from "./icon_button.module.scss"
import {RouteComponentProps, withRouter} from "react-router-dom";
import {useSelector} from "react-redux";
import {RootState} from "../../../redux/reducer";
import clsx from "clsx";

interface IIconButton extends RouteComponentProps {
    children: JSX.Element | JSX.Element[],
    url?: string,
    badge?: string,
    label?: string,
}

const IconButton = (props: IIconButton) => {
    const expanded = useSelector((state: RootState) => state.appbar.expanded)

    return (
        <div onClick={() => {
            props.url !== undefined && props.history.push(props.url)
        }} className={classes.root}>
            <div className={classes.animated}>
                <div className={classes.icon}>
                    {props.children}
                    {props.badge !== undefined && <div className={classes.badge}>{props.badge}</div>}
                </div>
                {props.label !== undefined &&
                <div className={clsx(classes.label, expanded ? classes.expanded : classes.collapsed)}>
                    {props.label}
                </div>
                }
            </div>
        </div>
    )
}

export default withRouter(IconButton);