import React from "react";
import classes from "./text_button.module.scss"
import clsx from "clsx";

type TMenuButton = {
    label: string
    onClick: () => void
    outline?: boolean
}

const TextButton = (props: TMenuButton) => {
    return(
        <div className={clsx(classes.text_button, props.outline && classes.outline)} onClick={props.onClick}>
            {props.label}
        </div>
    )
}

export default TextButton