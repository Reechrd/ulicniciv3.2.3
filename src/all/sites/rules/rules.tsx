import React from "react";
import classes from "./rules.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {State} from "../../redux/types";
import {setRule} from "../../redux/action_creators";
import TextButton from "../../components/buttons/text_button/text_button";

const Rules = () => {
    return(
        <div className={classes.root}>
            <div className={classes.main_box}>
                <Buttons />
            </div>
        </div>
    )
}

export default Rules


function Buttons() {
    const rule = useSelector((state: State)=>state.site.rule);
    const rules = useSelector((state: State)=>state.rules)

    return (
        <>
            <div className={classes.all_rule}>
                <div className={classes.number_below_rule}>
                    {rule}
                </div>
                <div className={classes.rule_box}>
                    {highlight(rules[rule - 1].text, rules[rule - 1].highlight)}
                </div>
            </div>
            <div className={classes.buttons}>
                <SimpleButtons />
            </div>
        </>
    );
}

const SimpleButtons = () => {
    const rule = useSelector((state: State)=>state.site.rule);
    const dispatch = useDispatch();

    const handleButtonChangeMinus = () => {
        let cislo = rule;
        if (cislo === 1) {
            cislo = 9;
        } else {
            cislo--;
        }
        dispatch(setRule(cislo));
    };
    const handleButtonChangePlus = () => {
        let cislo = rule;
        if (cislo === 9) {
            cislo = 1;
        } else {
            cislo++;
        }
        dispatch(setRule(cislo));
    };

    return (
        <>
            <TextButton label={"Před."} onClick={handleButtonChangeMinus}/>
            <div className={classes.buttons_spacer}/>
            <TextButton label={"Další"} onClick={handleButtonChangePlus}/>
        </>
    )
}

const highlight = (str: string, highlight: string) => <span>{str.split(highlight)[0]}<span style={{color: "#fd8f2a"}}>{highlight}</span>{str.split(highlight).pop()}</span>
