import React from "react";
import classes from "./messages.module.scss"
import {TMessage} from "../../redux/types";

const MessageForm = ({title, date, text, user}: TMessage) => (
    <div className={classes.message}>
        <div className={classes.left_text}>
            <h1>{title}</h1>
        </div>
        <div>
            <h3>{text}</h3>
        </div>
        <div className={classes.right_text}>
            <h2>{user} - {ChangeDate(date)}</h2>
        </div>
    </div>
)

function ChangeDate(date: string){
    let year = "";
    let mounth = "";
    let day = "";
    let dateFinal;

    for(let i=0; i < date.length; i++){
        if(i<4){
            year+=date.charAt(i);
        }else if(i===4){
        }else if(i===7){
        }else if(i<7){
            if(date.charAt(i)==="0" && i === 5){
            }else{
                mounth+=date.charAt(i);
            }
        }else if(i<10){
            if(date.charAt(i)==="0" && i === 8){
            }else{
                day+=date.charAt(i);
            }
        }
    }

    dateFinal = day + ". " + mounth + ". " + year;
    return dateFinal;
}

export default MessageForm