import React, {useEffect} from "react";
import classes from "./messages.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../redux/reducer";
import MessageForm from "./message_part";
import {setAccountNotification, setMessages} from "../../redux/action_creators";
import axios from "axios";
import {useSnackbar} from "notistack";

const Messages = () => {
    const messages = useSelector((state: RootState) => state.messages)
    const dispatch = useDispatch();
    const {enqueueSnackbar} = useSnackbar();
    const name = useSelector((state: RootState) => state.account.name);
    const key = useSelector((state: RootState)=> state.account.key);

    useEffect(() => {
        axios.get('http://localhost/php/get_messages.php').then(res=>{
            if(res.data === "Error1"){
                enqueueSnackbar('Nastal problém komunikace s databází. Zkuste to později', {
                    variant: 'error',
                });
            }else{
                const data = JSON.stringify(res.data)
                dispatch(setMessages(JSON.parse(data)));
                axios.post('http://localhost/php/set_notification.php', JSON.stringify({
                    name: name,
                    key: key,
                })).then(res2=>{
                    if(res2.data === "Error1"){
                        enqueueSnackbar('Nebylo možné zrušit upozornění.', {
                            variant: 'error',
                        });
                    }else{
                        dispatch(setAccountNotification(false))
                    }
                })
            }
        })
    }, [])

    return (
        <div className={classes.root_public}>
            <div className={classes.container_public}>
                <div className={classes.header_public}>
                    Zprávy
                </div>
                <div className={classes.content_public}>
                    {messages.slice(0).reverse().map((message) =>
                        <MessageForm title={message["title"]} date={message["date"]} text={message["text"]}
                                     user={message["user"]}/>
                    )}
                </div>
            </div>
        </div>
    )
}

export default Messages