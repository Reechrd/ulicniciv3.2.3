import React from "react";
import classes from "./entry.module.scss"
import clsx from "clsx";

type TProps = {
    children: JSX.Element | JSX.Element[],
    size?: "small" | "large"
}

const Entry = (props: TProps) => {
    return(
        <div className={classes.root}>
            <div className={clsx(classes.container, props.size === "large" ? classes.large : classes.small)}>
                {props.children}
            </div>
        </div>
    )
}

export default Entry