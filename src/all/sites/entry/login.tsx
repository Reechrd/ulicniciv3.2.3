import React, {useState} from "react";
import classes from "./entry.module.scss"
import Input from "../../components/input/input";
import Entry from "./entry";
import TextButton from "../../components/buttons/text_button/text_button";
import {useDispatch} from "react-redux";
import {setAccountKey, setAccountName, setAccountNotification} from "../../redux/action_creators";
import {useSnackbar} from "notistack";
import {RouteComponentProps, withRouter} from "react-router-dom";
import axios from "axios";
import {Check} from "../../backend/check";

const Login = (props: RouteComponentProps) => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const dispatch = useDispatch();
    const {enqueueSnackbar} = useSnackbar();

    const HandleLogin = () => {
        if (password === '' || username === '') {
            enqueueSnackbar('Žádné pole nesmí být prázdné.', {
                variant: 'error',
            });
        } else {
            axios.post('http://localhost/php/login.php', JSON.stringify({
                username: username.trim(),
                password: password.trim()
            })).then((res => {
                if (res.data === "Error1" || res.data === "") {
                    enqueueSnackbar('Nastal problém komunikace s databází. Zkuste to později', {
                        variant: 'error',
                    });
                } else if (res.data === "Error2") {
                    enqueueSnackbar('Něco se nezdařilo. Zkontrolujte zadané údaje.', {
                        variant: 'error',
                    });
                } else {
                    const data = res.data as string;
                    dispatch(setAccountKey(data));
                    dispatch(setAccountName(username))
                    enqueueSnackbar('Vaše přihlášení proběhlo úspěšně!.', {
                        variant: 'success',
                    });
                    props.history.push("/")
                }
            }))
        }
    }

    return (
        <Entry>
            <div className={classes.text}>Přihlášení</div>
            <Input type={"text"} placeholder={"Přezdívka"} content={username}
                   onChange={e => setUsername(e.target.value)}/>
            <Input type={"password"} placeholder={"Heslo"} content={password}
                   onChange={e => setPassword(e.target.value)}/>
            <TextButton outline={true} label={"Přihlásit"} onClick={HandleLogin}/>
            <div className={classes.spacer}/>
        </Entry>
    )
}

export default withRouter(Login)