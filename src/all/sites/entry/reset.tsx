import React from "react";
import {useParams} from 'react-router-dom'
import Entry from "./entry";

interface RouteParams {
    id: string
}

const Reset = () => {
    const {id} = useParams<RouteParams>()

    return (
        <Entry>
            <div>
                {id}
            </div>
        </Entry>
    )
}

export default Reset