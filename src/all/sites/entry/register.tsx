import React, {useState} from "react";
import classes from "./entry.module.scss"
import Input from "../../components/input/input";
import TextButton from "../../components/buttons/text_button/text_button";
import Entry from "./entry";
import {useDispatch} from "react-redux";
import {useSnackbar} from "notistack";
import axios from "axios";
import {setAccountKey, setAccountName} from "../../redux/action_creators";
import {RouteComponentProps} from "react-router-dom";

const Register = (props: RouteComponentProps) => {
    const [username, setUsername] = useState("")
    const [mail, setMail] = useState("")
    const [password, setPassword] = useState("")
    const [password_again, setPasswordAgain] = useState("")

    const dispatch = useDispatch();
    const {enqueueSnackbar} = useSnackbar();

    let re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

    const HandleRegister = () => {
        if (password === '' || username === '' || mail===""||password_again==="") {
            enqueueSnackbar('Žádné pole nesmí být prázdné.', {
                variant: 'error',
            });
        } else if(password !== password_again){
            enqueueSnackbar('Hesla se neshodují.', {
                variant: 'error',
            });
        }else if (!re.test(mail)) {
            enqueueSnackbar('Email není zadán správně.', {
                variant: 'error',
            });
        } else {
            axios.post('http://localhost/php/register.php', JSON.stringify({
                username: username.trim(),
                password: password.trim(),
                mail: mail.trim()
            })).then((res => {
                if (res.data === "Error1" || res.data === "") {
                    enqueueSnackbar('Nastal problém komunikace s databází. Zkuste to později', {
                        variant: 'error',
                    });
                } else if (res.data === "Error2") {
                    enqueueSnackbar('Uživatelské jméno již někdo používá.', {
                        variant: 'error',
                    });
                } else if (res.data==="Success"){
                    enqueueSnackbar('Vaše registrace proběhla úspěšně!.', {
                        variant: 'success',
                    });
                    props.history.push("/login")
                }else{
                    enqueueSnackbar('Neznámá chyba. Obraťte se na správce.', {
                        variant: 'error',
                    });
                }
            }))
        }
    }

    return (
        <Entry size={"large"}>
            <div className={classes.text}>Registrace</div>
            <Input type={"text"} placeholder={"Přezdívka"} content={username}
                   onChange={e => setUsername(e.target.value)}/>
            <Input type={"text"} placeholder={"E-mail"} content={mail} onChange={e => setMail(e.target.value)}/>
            <Input type={"password"} placeholder={"Heslo"} content={password}
                   onChange={e => setPassword(e.target.value)}/>
            <Input type={"password"} placeholder={"Heslo Znovu"} content={password_again}
                   onChange={e => setPasswordAgain(e.target.value)}/>
            <TextButton outline={true} label={"Registrovat"} onClick={HandleRegister}/>
        </Entry>
    )
}

export default Register