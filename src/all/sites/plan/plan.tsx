import React from "react";
import classes from "./plan.module.scss"
import PlanPart from "./plan_part";
import {useSelector} from "react-redux";
import {RootState} from "../../redux/reducer";

const Plan = () => {
    const steps = useSelector((state: RootState) => state.plan.steps)

    return(
        <div className={classes.root_public}>
            <div className={classes.container_public}>
                <div className={classes.header_public}>
                    Plán
                </div>
                <div className={classes.content_public}>
                    {steps.map((step, index)=>{
                        return (
                            <PlanPart number={index} text={step}/>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

export default Plan;