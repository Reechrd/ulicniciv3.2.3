import React from "react";
import classes from "./plan.module.scss"
import {useSelector} from "react-redux";
import {RootState} from "../../redux/reducer";
import clsx from "clsx";

type TPlanPart = {
    number: number,
    text: string
}

const PlanPart = (props: TPlanPart) => {
    const active_step = useSelector((state: RootState) => state.plan.active_step)
    const active_step_content = useSelector((state: RootState) => state.plan.active_step_content)

    return (
        <div className={classes.part_root}>
            <div className={classes.icon_container}>
                <div
                    className={clsx(classes.icon, props.number < active_step ? classes.done : props.number === active_step ? classes.ongoing : classes.tobedone)}>
                    {
                        props.number < active_step ? "✔" : props.number + 1
                    }
                </div>
            </div>
            <div className={classes.text_container}>
                {props.text}
                {props.number === active_step &&
                <div className={classes.info_text}>
                    {active_step_content}
                </div>
                }
            </div>
        </div>
    )
}

export default PlanPart