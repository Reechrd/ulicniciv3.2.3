import React from "react";
import classes from "./not_found.module.scss"
import TextButton from "../../components/buttons/text_button/text_button";
import {RouteComponentProps, withRouter} from "react-router-dom";

const NotFound = (props: RouteComponentProps) => {

    return (
        <div className={classes.root}>
            <div className={classes.container}>
                <div className={classes.main}>
                    Stránka nenalezena
                </div>
                <TextButton label={"Zpět domů"} onClick={()=> {
                    props.history.push("/")
                }}/>
            </div>
        </div>
    )
}

export default withRouter(NotFound);