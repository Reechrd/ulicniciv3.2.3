import React from "react";
import classes from "./maintenance.module.scss"

const Maintenance = () => {
    return (
        <div className={classes.root}>
            <h1 className={classes.text}>
                Probíhá údržba
            </h1>
        </div>
    )
}

export default Maintenance;