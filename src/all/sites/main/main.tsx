import React, {useEffect} from "react";
import classes from "./main.module.scss"
import {Helmet} from 'react-helmet'
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../redux/reducer";
import axios from "axios";
import {setAccountNotification} from "../../redux/action_creators";

const Main = () => {
    const name = useSelector((state: RootState) => state.account.name);
    const key = useSelector((state: RootState)=> state.account.key);
    const dispatch = useDispatch();
    
    useEffect(()=>{
        axios.post('http://localhost/php/check.php', JSON.stringify({
            name: name,
            key: key,
            type: "notification"
        })).then((res => {
            dispatch(setAccountNotification(res.data===1))
        }))
    },[])

    return (
        <>
            <Helmet>
                <title>Hlavní stránka</title>
            </Helmet>
            <div className={classes.root}>
                <div className={classes.container}>
                    <div className={classes.main}>
                        ULIČNÍCI
                    </div>
                    <div className={classes.secondary}>
                        #UR8Y0202
                    </div>
                </div>
            </div>
        </>
    )
}

export default Main