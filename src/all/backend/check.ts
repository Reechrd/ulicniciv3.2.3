import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../redux/reducer";
import axios from "axios";

 export const Check = (type: "logged" | "notification" | "admin") =>  {
    const name = useSelector((state: RootState) => state.account.name);
    const key = useSelector((state: RootState)=> state.account.key);

    axios.post('http://localhost/php/check.php', JSON.stringify({
        name: name,
        key: key,
        type: type
    })).then((res => {
        return res.data as string;
    }))
}