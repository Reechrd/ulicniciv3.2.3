import {
    ActionSetAccountKey, ActionSetAccountMail, ActionSetAccountName, ActionSetAccountNotification, ActionSetAccountTag,
    ActionSetMessages,
    ActionSetRule,
    ActionToggleAppbarExpanded,
    ActionToggleAppbarMenu,
    TMessage
} from "./types";

export const toggleAppbarExpanded = (): ActionToggleAppbarExpanded => ({
    type: "ACTION_TOGGLE_APPBAR_EXPANDED",
})

export const toggleAppbarMenu = (): ActionToggleAppbarMenu => ({
    type: "ACTION_TOGGLE_APPBAR_MENU"
})

export const setRule = (rule: number): ActionSetRule => ({
    type: "ACTION_SET_RULE",
    rule
})

export const setMessages = (messages: TMessage[]): ActionSetMessages => ({
    type: "ACTION_SET_MESSAGES",
    messages
})

export const setAccountKey = (key: string): ActionSetAccountKey => ({
    type: "ACTION_SET_ACCOUNT_KEY",
    key
})

export const setAccountName = (name: string): ActionSetAccountName => ({
    type: "ACTION_SET_ACCOUNT_NAME",
    name
})

export const setAccountTag = (tag: string): ActionSetAccountTag => ({
    type: "ACTION_SET_ACCOUNT_TAG",
    tag
})

export const setAccountMail = (mail: string): ActionSetAccountMail => ({
    type: "ACTION_SET_ACCOUNT_MAIL",
    mail
})

export const setAccountNotification = (notification: boolean): ActionSetAccountNotification => ({
    type: "ACTION_SET_ACCOUNT_NOTIFICATION",
    notification
})