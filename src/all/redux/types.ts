export type State = {
    appbar: TAppbar,
    site: TSite,
    rules: TRule[],
    plan: TPlan,
    messages: TMessage[],
    account: TAccount
}

export type TAccount = {
    key: string,
    name: string,
    mail: string,
    tag: string,
    notification: boolean
}

export type TAppbar = {
    expanded: boolean,
    menu: boolean,
}

export type TSite = {
    maintenance: boolean,
    rule: number
}

export type TRule = {
    text: string,
    highlight: string,
}

export type TPlan = {
    active_step: number,
    steps: string[],
    active_step_content: string,
}

export type TMessage = {
    title: string,
    date: string,
    text: string,
    user: string,
}

export type ActionToggleAppbarExpanded = {
    type: "ACTION_TOGGLE_APPBAR_EXPANDED",
}

export type ActionToggleAppbarMenu = {
    type: "ACTION_TOGGLE_APPBAR_MENU"
}

export type ActionSetRule = {
    type: "ACTION_SET_RULE",
    rule: number
}

export type ActionSetMessages = {
    type: "ACTION_SET_MESSAGES",
    messages: TMessage[]
}

export type ActionSetAccountKey= {
    type: "ACTION_SET_ACCOUNT_KEY",
    key: string,
}

export type ActionSetAccountName = {
    type: "ACTION_SET_ACCOUNT_NAME",
    name: string
}

export type ActionSetAccountMail = {
    type: "ACTION_SET_ACCOUNT_MAIL",
    mail: string
}

export type ActionSetAccountTag = {
    type: "ACTION_SET_ACCOUNT_TAG",
    tag: string
}

export type ActionSetAccountNotification = {
    type: "ACTION_SET_ACCOUNT_NOTIFICATION",
    notification: boolean
}

export type Actions =
    ActionToggleAppbarExpanded
    | ActionToggleAppbarMenu
    | ActionSetRule
    | ActionSetMessages
    | ActionSetAccountMail
    | ActionSetAccountKey
    | ActionSetAccountTag
    | ActionSetAccountName
    | ActionSetAccountNotification