import {Actions, State} from "./types";
import {initialState} from "./initial_state";
import {createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {DeepReadonly} from "ts-essentials";

export const reducer = (state: State = initialState, action: Actions): State => {
    switch (action.type) {
        case "ACTION_SET_RULE":
            return {
                ...state,
                site: {
                    ...state.site,
                    rule: action.rule
                }
            }
        case "ACTION_TOGGLE_APPBAR_MENU":
            return {
                ...state,
                appbar: {
                    ...state.appbar,
                    menu: !state.appbar.menu,
                }
            }
        case "ACTION_TOGGLE_APPBAR_EXPANDED":
            return {
                ...state,
                appbar: {
                    ...state.appbar,
                    expanded: !state.appbar.expanded,
                }
            }
        case "ACTION_SET_MESSAGES":
            return {
                ...state,
                messages: action.messages
            }
        case "ACTION_SET_ACCOUNT_KEY":
            return {
                ...state,
                account: {
                    ...state.account,
                    key: action.key
                }
            }
        case "ACTION_SET_ACCOUNT_MAIL":
            return {
                ...state,
                account: {
                    ...state.account,
                    mail: action.mail
                }
            }
        case "ACTION_SET_ACCOUNT_NAME":
            return {
                ...state,
                account: {
                    ...state.account,
                    name: action.name
                }
            }
        case "ACTION_SET_ACCOUNT_TAG":
            return {
                ...state,
                account: {
                    ...state.account,
                    tag: action.tag
                }
            }
        case "ACTION_SET_ACCOUNT_NOTIFICATION":
            return {
                ...state,
                account: {
                    ...state.account,
                    notification: action.notification
                }
            }
        default:
            return state;
    }
}

export const store = createStore(reducer, composeWithDevTools())

export type RootState = DeepReadonly<ReturnType<typeof store.getState>>