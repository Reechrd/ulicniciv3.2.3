import {State} from "./types.js"

export const initialState: State = {
    appbar: {
        expanded: false,
        menu: false,
    },
    site: {
        maintenance: false,
        rule: 1
    },
    rules: [
        {text: "Přijímáme hráče co mají rádi kolektivní hru a hrají CW,CWL a CG", highlight: "CW,CWL a CG"},
        {text: "CW hraje každý kdo je zapnutý a dodržuje pravidla klanu", highlight: "CW hraje"},
        {text: "Kdo je zapnutý, ten je povinen dát vždy oba útoky!", highlight: "oba útoky"},
        {text: "Útočí se podle soupeře, ale většinou níže a na jistotu", highlight: "jistotu"},
        {text: "Nedobité vesnice opravujeme", highlight: "opravujeme"},
        {text: "Kdo nechce, nebo ví, že nebude moci útočit, ten se z CW vypne!", highlight: "CW vypne"},
        {text: "Do CW se chodí vždy s hrdiny a plným CC!", highlight: "vždy s hrdiny a plným CC"},
        {text: "Donate je dobrovolný", highlight: "Donate"},
        {text: "Elder je za aktivitu, CW a donate", highlight: "Elder"},
    ],
    plan: {
        active_step: 2,
        active_step_content: "Obnovení hesla přídá možnost obnovit heslo pomocí emailu, který jste zadali při registraci na stránkách. Tato stránka bude k nalezení při přihlašování.",
        steps: [
            'Stránka prestiže',
            'Systém zpráv + novinky na stránkách',
            "Možnost obnovení hesla",
            "Stránka pro kontaktování leadera a správce",
            "Statistiky pro celý klan",
            "Statistiky pro jednotlivce",
            "Grey list",
            "Stránka pro hlasování",
            "Stránka s užitečnými odkazy",
            "Fórum"
        ],
    },
    messages: [
        {
            title: "TITLE 1",
            date: "2003-12-19",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim a",
            user: "TESTER1"
        },
        {
            title: "TITLE 2",
            date: "2003-04-01",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim a",
            user: "TESTER2"
        },
        {
            title: "TITLE 3",
            date: "2003-09-12",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim a",
            user: "TESTER3"
        },
        {
            title: "TITLE 4",
            date: "2003-11-28",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim a",
            user: "TESTER4"
        }
    ],
    account: {
        key: "",
        name: "",
        mail: "",
        tag: "",
        notification: false
    }
}