import React from 'react';
import "./App.css"
import Appbar from "./all/components/appbar/appbar";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Main from "./all/sites/main/main";
import {useSelector} from "react-redux";
import {RootState} from "./all/redux/reducer";
import Maintenance from "./all/sites/maintenance/maintenance";
import NotFound from "./all/sites/not_found/not_found";
import Rules from "./all/sites/rules/rules";
import Login from "./all/sites/entry/login";
import Register from "./all/sites/entry/register";
import Forgot from "./all/sites/entry/forgot";
import Reset from "./all/sites/entry/reset";
import Plan from "./all/sites/plan/plan";
import Messages from "./all/sites/messages/messages";

const App = () => {
  const maintenance = useSelector((state: RootState) => state.site.maintenance);

  if (maintenance) {
    return <Maintenance/>
  } else {
    return (
        <>
          <Router>
            <Appbar/>
            <Switch>
              <Route exact path={"/"} component={Main}/>
              <Route path={"/rules"} component={Rules}/>
              <Route path={"/login"} component={Login}/>
              <Route path={"/register"} component={Register}/>
              <Route path={"/forgot"} component={Forgot}/>
              <Route path={"/reset/:id"} component={Reset}/>
              <Route path={"/todo"} component={Plan}/>
              <Route path={"/messages"} component={Messages}/>
              <Route component={NotFound}/>
            </Switch>
          </Router>
        </>
    )
  }
}

export default App;